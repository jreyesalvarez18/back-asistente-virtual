const express = require('express')
require('dotenv').config()
const cors = require('cors')
const app = express() // servidor de express

// directorio publico
app.use( express.static('public') )

app.use(cors())

//lectura y parse de body
app.use(express.json())

//TODO auth // crear, renew
app.use('/api/user', require('./routes/user'))
//TODO CRUD: EVENTOS 


app.listen(process.env.PORT, ()=>{
    console.log("Sirviendo en puerto 4500");
})