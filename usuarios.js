const usuarios = [
    {
        message: "hola",
        timestamp: '08/03/2022',
        role: 'user',
        attachments:[],
        _id:10
    },
    {
        message: "hola, en que puedo ayudarte??",
        timestamp: '08/03/2022',
        role: 'agent',
        attachments:[],
        _id:9
    },
    {
        message: "el día de ayer compre un mause gamer, y según la descripción el producto deberia conectarse de forma inalámbrica, he probado con dos computadores y aún asi no funciona",
        timestamp: '08/03/2022',
        role: 'user',
        attachments:[],
        _id:8
    },
    {
        message: "Hagamos lo siguiente, mañana probare las configuraciones de pc para poder indicarte y ver si funcionan",
        timestamp: '08/03/2022',
        role: 'agent',
        attachments:[],
        _id:7
    },    
    {
        message: "Oka, estoy atento a las intrucciones",
        timestamp: '08/03/2022',
        role: 'user',
        attachments:[],
        _id:6
    },
    {
        message: "Estoy probando el control, te aviso como me va",
        timestamp: '08/03/2022',
        role: 'agent',
        attachments:[],
        _id:5
    },
    {
        message:" mira, logre conectatrlo de la siguiente manera, 1.- active el Bluetooth del pc 2.- mantuve pulsado el botón azul con la flecha entre los análogos hasta que apareció el control! 3.- seleccione el control y quedo listo!!",
        timestamp: '08/03/2022',
        role: 'agent',
        attachments:[],
        _id:4
    },
    {
        message: "Hola, hice los pasos que me dices, y el pc solo empareja el mause pero aun asi no funciona de forma inalámbrica,",
        timestamp: '08/03/2022',
        role: 'user',
        attachments:[],
        _id:3
    },
    {
        message: "Estmiado cliente, acabamos de confirmar y ese control para pc solo funciona con cable",
        timestamp: '08/03/2022',
        role: 'agent',
        attachments:[],
        _id:2
    },
    {
        message: "Procederemos a hacer la devolución del dinero",
        timestamp: '08/03/2022',
        role: 'agent',
        attachments:[
            {
                name: 'prueba archivo',
                contentType: 'image/png',
                content: 'iVBORw0KGgoAAAANSUhEUgAAACQAAAAXCAYAAABj7u2bAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAFaSURBVHgBtZYNbcMwEEYvQ1AIYbBCyBiUwQKhDJohWBlEQ1ANgTsEhWAIG4Nvd7WjupFr139PujZxJPvpbJ2PKAEABw6FemiOmaNP8RCRDccFbXl3FxvkPyA0oz2/kEzxz8kOaI69R6ZPmLCUI3kGNcfoCO0RZ4LJtEYZmgIfJXOSnSMiMo78Dg2FFkJbMXu2WKFA6IXibALffjxjX5RP/4xQCJ/sKxXQSZ4on3PXdW/LC0yB01RAaYYG3FfZPxvZlAoJn8sDZ0tkPqgE1GG3mnNCJrWETNkPS8ldqGMT1RKCXWwt1VsR5YyNCIjVFPJKPTgmIjrlCqVemjpBaj13sFKfOaTGfFMaPYdIHeixjBTUE/kKq8dS9nxwvscu11i2RjdjuJ0rHxf3MhQxXz80oA4KptGLX9b8sEW4Y1Roj8azvTXa99SStS2lAnMeFMo7Q9g5JK7d5rLGPz540p+3rLoqAAAAAElFTkSuQmCC'
            }
        ],
        _id:1
    }

]

module.exports = usuarios