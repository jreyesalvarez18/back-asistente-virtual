const { response, request } = require('express')
let  usuarios  = require('../usuarios')


const usuariosGet = (request, response) => {
    const query = request.query
    setTimeout(()=>{
        response.json(usuarios)
    },2000)
}
const usuariosPost = (request, response) => {
    const body = request.body
    usuarios = [
        ...usuarios,
        body
    ]
    response.json(body)
}
const usuariosPut = (request, response) => {
    const id = request.params.id

    response.json({
        id
    })
}
const usuariosDelete = (request, response) => {
    response.json({
        msg: "Delete API"
    })
}

module.exports = {
    usuariosGet,
    usuariosPut,
    usuariosPost,
    usuariosDelete
}