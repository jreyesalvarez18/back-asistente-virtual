/**
 * rutas de usuario auth
 */

 const { Router } = require('express')
 const { usuariosGet, usuariosPut, usuariosPost, usuariosDelete } = require('../controllers/user')
 
 const router = Router()
 
 router.get('/get', usuariosGet)
 router.post('/add', usuariosPost)
 router.put('/upd', usuariosPut)
 router.delete('/delete/:id',usuariosDelete)
 
 module.exports = router